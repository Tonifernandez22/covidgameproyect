﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BillMovement : MonoBehaviour
{
    public GameObject BillOnScreen;
    float Speed;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Speed = Time.time;
        gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, new Vector2(-8.17f, 4.58f), 20.0f * Time.deltaTime);

        Vector2 currentPos = gameObject.transform.position;
        
        if (currentPos.x == -8.17f && currentPos.y == 4.58f)
        {
            Destroy(this.gameObject);
        }
    }
}
