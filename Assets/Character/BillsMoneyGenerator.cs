﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BillsMoneyGenerator : MonoBehaviour
{
    public GameObject MoneyBill;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void SpawnMoney(int MoneyGenerated)
    {
        int Debt = MoneyGenerated;

        for (int i = 0; i < MoneyGenerated; i++)
        {
            Instantiate(MoneyBill, new Vector2( Random.Range(gameObject.transform.position.x + 0.25f, gameObject.transform.position.x - 0.25f), 
                                                Random.Range(gameObject.transform.position.y + 0.25f, gameObject.transform.position.y - 0.25f)), 
                                                Quaternion.identity );
        }
    }
}
