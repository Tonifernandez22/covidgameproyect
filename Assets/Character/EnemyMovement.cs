﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyMovement : MonoBehaviour
{
    public float Speed;
    Text livesText;
    Text moneyText;

    // Start is called before the first frame update
    void Start()
    {
        //livesText = GameObject.FindGameObjectWithTag("LiveText").GetComponent<Text>();
        moneyText = GameObject.FindGameObjectWithTag("MoneyText").GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.Translate(Vector2.left * Speed * Time.deltaTime);

        if (gameObject.transform.position.x < -10.0f)
        {
            //We get the numerical value of a UI.text
            int currentMoney = int.Parse(moneyText.text);

            //We substract the desired value from the text string component
            if (currentMoney >= 5)
            {
                currentMoney -= 5;
                moneyText.text = currentMoney.ToString("F0");

            } else if (currentMoney < 5)
            {
                currentMoney -= Mathf.Abs(0 - currentMoney);
                moneyText.text = currentMoney.ToString("F0");
            }

            Destroy(this.gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.gameObject.name == "PoliceCar1")
        {
            Physics.IgnoreCollision(collision.collider.gameObject.GetComponent<Collider>(), GetComponent<Collider>());
        }
    }
}
