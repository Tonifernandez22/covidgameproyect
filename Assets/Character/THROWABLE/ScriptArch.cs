﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScriptArch : MonoBehaviour
{
    Vector3 policeCarPosition;
    Vector3 policeCarPositionRelative;

    Vector3 spawnPosition;
    Vector3 spawnPositionRelative;

    Vector3 center;

    public Vector3 midPoint;
    float startTime;

    Text livesText;

    // Start is called before the first frame update
    void Start()
    {
        livesText = GameObject.FindGameObjectWithTag("LiveText").GetComponent<Text>();

        policeCarPosition = GameObject.FindGameObjectWithTag("PoliceCar").transform.position;
        spawnPosition = gameObject.transform.position;

        startTime = Time.time;

        center = Vector3.Lerp(policeCarPosition, spawnPosition, 0.5f);
        center -= new Vector3(0, 5, 0);

        policeCarPositionRelative = policeCarPosition - center;
        spawnPositionRelative = spawnPosition - center;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.Slerp(spawnPositionRelative, policeCarPositionRelative, (Time.time - startTime) / 2);
        transform.position += center;

        if (Vector3.Distance(gameObject.transform.position, policeCarPosition) < 0.1f)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.gameObject.tag == "PoliceCar")
        {
            //We get the numerical value of a UI.text
            int currentLives = int.Parse(livesText.text);

            //We substract the desired value from the text string component
            currentLives -= 1;

            //We update the string value onscreen
            livesText.text = currentLives.ToString("F0");

            Destroy(this.gameObject);

            Destroy(gameObject);
        }
    }
}
