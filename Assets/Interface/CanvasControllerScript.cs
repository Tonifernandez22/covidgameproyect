﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasControllerScript : MonoBehaviour
{
    Touch touch;
    Vector2 startTouchPosition, endTouchPosition;

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            touch = Input.GetTouch(0);

            switch (touch.phase)
            {
                //When we first touch the screen
                case TouchPhase.Began:
                    startTouchPosition = touch.position;
                    break;

                //When we stop touching the screen
                case TouchPhase.Ended:
                    endTouchPosition = touch.position;

                    if (endTouchPosition.y < startTouchPosition.y)
                    {
                        //Make the UI visible
                        //Make the UI fall from the top of the screen...
                        Debug.Log("hh");
                    }

                    if (startTouchPosition.y < endTouchPosition.y)
                    {
                        //Make the UI invisible
                        //Make the UI rise to the top of the screen and hiding beyond...
                        Debug.Log("pp");
                    }

                    break;
            }
        }
    }
}
