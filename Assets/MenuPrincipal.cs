﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MenuPrincipal : MonoBehaviour
{
    bool isActive;

    public GameObject MuteButton, UnmuteButton;

    // Start is called before the first frame update
    void Start()
    {
        MuteButton.SetActive(true);
        UnmuteButton.SetActive(false);

        Musica.MusicaActivada = true;
        isActive = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Application.Quit();
        }
    }

    public void EmpezarJuego()
    {
        SceneManager.LoadScene("SampleScene");
    }

    public void Mute()
    {
        MuteButton.SetActive(false);
        UnmuteButton.SetActive(true);
        Musica.MusicaActivada = false;
    }

    public void Unmute()
    {
        UnmuteButton.SetActive(false);
        MuteButton.SetActive(true);
        Musica.MusicaActivada = true;
    }

    public void Exit()
    {
        Application.Quit();
    }
}
