﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax_controller_script : MonoBehaviour
{
    //Speed that each layer of the background moves to the left (parallax effect)
    //[Header("Insert here the correspondant material to the parallax layer")]
    //public Material Parallax_Layer_Material;

    public float XSpeed;
    //SpriteRenderer spriteRenderer;
    Renderer rend;

    //Vector2 mainTextureOffsetIndicator;

    void Start()
    {
        //We select the material that the parallax layer will be using, allowing us to implement the offseting.
        //spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        //spriteRenderer.material = Parallax_Layer_Material;

        rend = GetComponent<Renderer>();
    }

    void Update()
    {
        //This variable we created is assigned to address the x,y value of the texture offset.
        //spriteRenderer.material.mainTextureOffset = mainTextureOffsetIndicator;

        //mainTextureOffsetIndicator.x = (Time.time * XSpeed) / 10;

        rend.material.mainTextureOffset = new Vector2(Time.time * XSpeed, 0);

    }
}
