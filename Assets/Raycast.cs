﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Raycast : MonoBehaviour
{
    public Text Mytexto;
    public Text MyTiempo;
    public int Dinero = 0;
    
    public LayerMask layermask;

    public GameObject Mask;

    public float timer;
    public string timerFormatted;

    public AudioClip sonidoCaja;
    public GameObject PoliceCar;

    // Start is called before the first frame update
    void Start()
    {
        Input.multiTouchEnabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0 && Input.touches[1].phase == TouchPhase.Began)
        {
            Ray raycastGeneration = Camera.main.ScreenPointToRay(Input.GetTouch(1).position);

            if (Physics.Raycast(raycastGeneration, out RaycastHit collision))
            {
                if (collision.collider.gameObject.tag == "Enemigo1")
                {
                    int currentMoney = int.Parse(Mytexto.text);   

                    int MoneyGenerated = Random.Range(4, 6);

                    currentMoney += MoneyGenerated;

                    if (Musica.MusicaActivada)
                    {
                        AudioSource.PlayClipAtPoint(sonidoCaja, PoliceCar.transform.position, 1f);
                    }

                    Mytexto.text = currentMoney.ToString();

                    collision.collider.gameObject.SendMessage("SpawnMoney", MoneyGenerated);

                    Instantiate(Mask, collision.collider.gameObject.transform.position, Quaternion.identity);

                    Destroy(collision.collider.gameObject);
                }
            }
        }

        /*
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
      
        if (Physics.Raycast(ray, out RaycastHit hit))
        {
            if (hit.collider.gameObject.tag == "Enemigo1" && Input.GetMouseButtonDown(0))
            {
                int currentMoney = int.Parse(Mytexto.text);

                int MoneyGenerated = Random.Range(4, 6);

                currentMoney += MoneyGenerated;

                AudioSource.PlayClipAtPoint(sonidoCaja, PoliceCar.transform.position, 1f);

                Mytexto.text = currentMoney.ToString();

                hit.collider.gameObject.SendMessage("SpawnMoney", MoneyGenerated);

                Instantiate(Mask, hit.collider.gameObject.transform.position, Quaternion.identity);

                Destroy(hit.collider.gameObject);
            }
        }
        */
        

        //MyTiempo.text = Time.timeSinceLevelLoad.ToString("F2") + " s";

        timer += Time.deltaTime;

        System.TimeSpan t = System.TimeSpan.FromSeconds(timer);

        timerFormatted = string.Format("{0:D2}h:{1:D2}m:{2:D2}s", t.Hours, t.Minutes, t.Seconds);

        MyTiempo.text = timerFormatted;
    }
}
