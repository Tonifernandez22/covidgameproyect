﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGenerator : MonoBehaviour
{
    public GameObject Enemy1, Enemy2, Enemy3;
    public List<GameObject> Enemies = new List<GameObject>();

    public GameObject GenPos1, GenPos2;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("GenerateChances", 2.0f, 1.0f);
        Enemies.Add(Enemy2);
        Enemies.Add(Enemy3);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void GenerateChances()
    {
        int Chance = Random.Range(0, 101);

        if (Chance <= 40)
        {
            //Instantiating in the upper section
            Instantiate(Enemy2, GenPos1.transform.position, Quaternion.identity);

        } else if (Chance > 60)
        {
            //Instantiatin in the lower section
            Instantiate(Enemy3, GenPos2.transform.position, Quaternion.identity);
        }
    }
}
