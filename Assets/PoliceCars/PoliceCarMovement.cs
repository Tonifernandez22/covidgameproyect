﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoliceCarMovement : MonoBehaviour
{
    public Joystick movementControl;
    Vector2 carMovement;
    Rigidbody rigid;
    AudioSource audioSource;

    void Start()
    {
        rigid = gameObject.GetComponent<Rigidbody>();

        audioSource = gameObject.GetComponent<AudioSource>();

        if (Musica.MusicaActivada)
        {
            audioSource.loop = true;
            audioSource.Play();
        }
    }

    void Update()
    {
        rigid.sleepThreshold = 0.0f;

        transform.position = new Vector2(Mathf.Clamp(transform.position.x, -4.5f, 4.0f), transform.position.y);

        //Converts the joysticks's vertical value to the horizontal value used for back and forth movement
        carMovement = new Vector3(movementControl.Vertical, 0);

        //Moves the car correspondingly based on the Joystick's vertical value.
        transform.Translate((carMovement * 10) * Time.deltaTime, Space.World);
    }
}
