﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DeathScreen : MonoBehaviour
{
    public Text livesText;
    public Text moneyText;
    public GameObject DeathMenu;
    public GameObject Highscore;
    public GameObject HighscoreText;
    public Text HighscoretextText;

    public int score = 0;
    public int highScore = 0;
    public string highScoreKey = "HighScore";

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1.0f;

        DeathMenu.SetActive(false);
        Highscore.SetActive(false);
        HighscoreText.SetActive(false);

        if (PlayerPrefs.HasKey("HighScore"))
        {
            highScore = PlayerPrefs.GetInt(highScoreKey, 0);
        }

        /*
        if (Musica.MusicaActivada == true)
        {
            AudioSource audioSource = gameObject.GetComponent<AudioSource>();
            audioSource.loop = true;
            audioSource.Play();
        }
        */
    }

    // Update is called once per frame
    void Update()
    {
        int currentLives = int.Parse(livesText.text);

        if (currentLives <= 0)
        {
            Time.timeScale = 0.0f;

            DeathMenu.SetActive(true);
            Highscore.SetActive(true);
            HighscoreText.SetActive(true);

            //Current score
            score = int.Parse(moneyText.text);

            if (score > highScore)
            {
                PlayerPrefs.SetInt(highScoreKey, score);
                PlayerPrefs.Save();

                HighscoretextText.text = (PlayerPrefs.GetInt(highScoreKey, 0)).ToString();

            } else if (score < highScore)
            {
                HighscoretextText.text = (PlayerPrefs.GetInt(highScoreKey, 0)).ToString();
            }
        }
    }

    public void Restart()
    {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }

    public void Exit()
    {
        SceneManager.LoadScene("Menu");
    }
}
